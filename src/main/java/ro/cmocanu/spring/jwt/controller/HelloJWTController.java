package ro.cmocanu.spring.jwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ro.cmocanu.spring.jwt.config.User;
import ro.cmocanu.spring.jwt.util.TokenHelper;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HelloJWTController {

    @Autowired
    private TokenHelper tokenHelper;


    @GetMapping("/api/hello")
    @ResponseBody
    public String hello(HttpServletRequest request) throws Exception {

        String token = tokenHelper.getToken(request);

        User user = tokenHelper.getUserInfo(token);

        return "Hello World " + user;
    }

}
