package ro.cmocanu.spring.jwt.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ro.cmocanu.spring.jwt.config.User;
import ro.cmocanu.spring.jwt.util.TokenHelper;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private TokenHelper tokenHelper;

    private static Map<String, UserDetails> userRepository;

    public CustomUserDetailsService() {
        userRepository = new HashMap<String, UserDetails>(5);

        GrantedAuthority USER = new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return "USER";
            }
        };

        GrantedAuthority ADMIN = new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return "ADMIN";
            }
        };

        userRepository.put("admin", new User("admin","admin", "Full Admin", Arrays.asList(ADMIN)));
        userRepository.put("user1", new User("user1","user1", "User 1", Arrays.asList(USER)));
        userRepository.put("user2", new User("user2","user2", "User 2", Arrays.asList(USER)));
        userRepository.put("user3", new User("user3","user3", "User 3", Arrays.asList(USER)));

    }

    @PostConstruct
    private void listTokens() {
        System.out.println("==========================");
        userRepository.forEach( (String username, UserDetails userDetails) -> {
            System.out.println(">>>> " + tokenHelper.generateToken(username, userDetails));
        } );
        System.out.println("==========================");
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if(userRepository.containsKey(username)) {
            return userRepository.get(username);
        } else {
            throw new UsernameNotFoundException(username);
        }
    }
}
