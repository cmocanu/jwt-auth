package ro.cmocanu.spring.jwt.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultHeader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ro.cmocanu.spring.jwt.config.User;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

@Component
public class TokenHelper {

    @Value("${app.name}")
    private String APP_NAME;

    @Value("${app.token.header}")
    private String AUTH_HEADER;

    @Value("${app.token.publicKey}")
    public String publicKeyStr;

    @Value("${app.token.privateKey}")
    public String privateKeyStr;

    public PublicKey publicKey;
    public PrivateKey privateKey;

    @Value("${app.token.expiration}")
    private int EXPIRES_IN;

    private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.RS256;

    @PostConstruct
    private void initKeys() {
        publicKey = null;
        publicKeyStr = publicKeyStr.replace("-----BEGIN PUBLIC KEY-----","").replace("-----END PUBLIC KEY-----","").replaceAll("\n","");
        privateKeyStr = privateKeyStr.replace("-----BEGIN PRIVATE KEY-----","").replace("-----END PRIVATE KEY-----","").replaceAll("\n","");

        try {

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            X509EncodedKeySpec x509Spec=new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyStr));
            publicKey = keyFactory.generatePublic(x509Spec);

            PKCS8EncodedKeySpec pkcs8Spec=new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyStr));
            privateKey = keyFactory.generatePrivate(pkcs8Spec);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }

    public String getAuthHeaderFromHeader( HttpServletRequest request ) {
        return request.getHeader(AUTH_HEADER);
    }

    public String getToken( HttpServletRequest request ) {
        /**
         *  Getting the token from Authentication header
         *  e.g Bearer your_token
         */
        String authHeader = getAuthHeaderFromHeader( request );
        if ( authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return null;
    }

    private Claims getAllClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(publicKey)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public User getUserInfo(String token) {
        User user = null;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            Map userMap = claims.get("userInfo", Map.class);
            if (userMap != null)
                user = new User(userMap.get("username").toString(), null, userMap.get("name").toString(), null);
        } catch (Exception e) {
            user = null;
        }
        return user;
    }

    public Date getIssuedAtDateFromToken(String token) {
        Date issueAt;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            issueAt = claims.getIssuedAt();
        } catch (Exception e) {
            issueAt = null;
        }
        return issueAt;
    }

    public Date getExpirationDateFromToken(String token) {
        Date issueAt;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            issueAt = claims.getExpiration();
        } catch (Exception e) {
            issueAt = null;
        }
        return issueAt;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        User user = (User) userDetails;
        final String username = getUsernameFromToken(token);
        final Date created = getIssuedAtDateFromToken(token);
        final Date expiration = getExpirationDateFromToken(token);
        final Date now = new Date( System.currentTimeMillis() );

        return (
                username != null &&
                        username.equals(userDetails.getUsername()) &&
                        now.before(expiration) &&
                        now.after(created)
        );
    }

    private Date generateExpirationDate() {
        return new Date((new Date()).getTime() + EXPIRES_IN * 1000);
    }

    public String refreshToken(String token) {
        String refreshedToken;
        Date a = new Date();
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            claims.setIssuedAt(a);
            refreshedToken = Jwts.builder()
                    .setClaims(claims)
                    .setExpiration(generateExpirationDate())
                    .signWith( SIGNATURE_ALGORITHM, privateKey )
                    .compact();
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public String generateToken(String username, UserDetails user) {
        Header header = new DefaultHeader();
        header.setType(Header.JWT_TYPE);

        return Jwts.builder()
                .setHeaderParams(header)
                .setIssuer(APP_NAME)
                .setSubject(username)
                .setAudience("GA") // something relevant
                .setIssuedAt(new Date())
                .setExpiration(generateExpirationDate())
                .claim("userInfo", user)
                .signWith(SIGNATURE_ALGORITHM, privateKey)
                .compact();
    }


}
